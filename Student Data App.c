#include <stdio.h>
#include <string.h>

// Define the structure
typedef struct {
    char name[51]; // Student's name, max 50 characters
    char dob[11]; // Date of birth in YYYY-MM-DD format, max 10 characters
    char reg_num[7]; // Registration number, 6 numerical characters
    char prog_code[5]; // Program code, max 4 characters
    double annual_tuition; // Annual tuition, a non-zero positive number
} Student;

// Define a constant for the maximum number of students
#define MAX_STUDENTS 100

// Declare an array of students
Student students[MAX_STUDENTS];

// Declare a variable to keep track of the number of students
int num_students = 0;

// Define a function for each operation
void create_student(Student* students, int*num_students) {
    if (*num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n\n");
        return;
    }

    do {
        printf("Enter student's name: ");
        scanf("%s", students[*num_students].name);
        if(strlen(students[*num_students].name) <= 0 | strlen(students[*num_students].name) > 50) {
            printf("Student's name must be 50 characters or less. Please try again.\n");
        }
    } while(strlen(students[*num_students].name) > 50);

    do {
       printf("Enter student's date of birth (YYYY-MM-DD): ");
        scanf("%s", students[*num_students].dob);
        if(strlen(students[*num_students].dob) != 10) {
            printf("Student's Date of Birth must be 10 characters. Please try again.\n");
        }
    } while(strlen(students[*num_students].dob) != 10);

    do {
        printf("Enter student's registration number: ");
        scanf("%s", students[*num_students].reg_num);
        if(strlen(students[*num_students].reg_num) > 6) {
            printf("Student's Registration Number must be 6 digits. Please try again.\n");
        }
    } while(strlen(students[*num_students].reg_num) > 6 | strlen(students[*num_students].reg_num) <= 0);

    do {
        printf("Enter student's program code: ");
        scanf("%s", students[*num_students].prog_code);
        if(strlen(students[*num_students].prog_code) > 4 ) {
            printf("Student's Registration Number must be 6 digits. Please try again.\n");
        }
    } while(strlen(students[*num_students].prog_code) > 4 );

    do {
        printf("Enter student's annual tuition: ");
        scanf("%lf", &students[*num_students].annual_tuition);
        if(students[*num_students].annual_tuition <= 0) {
            printf("Annual tuition should be greater than zero. Please try again.\n");
        }
    } while(students[*num_students].annual_tuition <= 0);
    printf("Student created successfully.\n\n");
    (*num_students)++;
}

void read_student(Student* students, int num_students) {
    if(num_students == 0) {
        printf("No students to display.\n\n");
        return;
    }
    printf("Student List\n\n");
    for(int i = 0; i < num_students; i++) {
        printf("Student %d\n", i+1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dob);
        printf("Registration Number: %06s\n", students[i].reg_num);
        printf("Program Code: %s\n", students[i].prog_code);
        printf("Annual Tuition: %.2lf\n\n", students[i].annual_tuition);
    }
}

void update_student(Student* students, int num_students) {
    if(num_students == 0) {
        printf("No students to update.\n\n");
        return;
    }
    char reg_num[7];
    printf("Enter the registration number of the student to update: ");
    scanf("%s", reg_num);
    for(int i = 0; i < num_students; i++) {
        if(strcmp(students[i].reg_num, reg_num) == 0) {

            do {
                printf("Enter updated student's name: ");
                scanf("%s", students[i].name);
                if(strlen(students[i].name) <= 0 | strlen(students[i].name) > 50) {
                    printf("Student's name must be 50 characters or less. Please try again.\n");
                }
            } while(strlen(students[i].name) > 50);

            do {
               printf("Enter updated student's date of birth (YYYY-MM-DD): ");
                scanf("%s", students[i].dob);
                if(strlen(students[i].dob) != 10) {
                    printf("Student's Date of Birth must be 10 characters. Please try again.\n");
                }
            } while(strlen(students[i].dob) != 10);
            
            do {
                printf("Enter updated student's program code: ");
                scanf("%s", students[i].prog_code);
                if(strlen(students[i].prog_code) > 4 ) {
                    printf("Student's Registration Number must be 6 digits. Please try again.\n");
                }
            } while(strlen(students[i].prog_code) > 4 );

            do {
                printf("Enter updated student's annual tuition: ");
                scanf("%lf", &students[i].annual_tuition);
                if(students[i].annual_tuition <= 0) {
                    printf("Annual tuition should be greater than zero. Please try again.\n");
                }
            } while(students[i].annual_tuition <= 0);
            printf("Student updated successfully.\n\n");
            return;
        }
    }
    printf("No student found with the given registration number.\n\n");
}

void delete_student(Student* students, int* num_students) {
    if(*num_students == 0) {
        printf("No students to delete.\n\n");
        return;
    }
    char reg_num[7];
    printf("Enter the registration number of the student to delete: ");
    scanf("%s", reg_num);
    for(int i = 0; i < *num_students; i++) {
        if(strcmp(students[i].reg_num, reg_num) == 0) {
            for(int j = i; j < *num_students - 1; j++) {
                students[j] = students[j+1];
            }
             printf("Student deleted successfully.\n\n");
            (*num_students)--;
            return;
        }
    }
    printf("No student found with the given registration number.\n\n");
}

void search_student(Student* students, int num_students) {
    if(num_students == 0) {
        printf("No students to search.\n");
        return;
    }
    char reg_num[7];
    printf("Enter the registration number of the student to search: ");
    scanf("%s", reg_num);
    for(int i = 0; i < num_students; i++) {
        if(strcmp(students[i].reg_num, reg_num) == 0) {
            printf("Student found:\n\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Registration Number: %s\n", students[i].reg_num);
            printf("Program Code: %s\n", students[i].prog_code);
            printf("Annual Tuition: %.2lf\n\n", students[i].annual_tuition);
            return;
        }
    }
    printf("No student found with the given registration number.\n\n");
}


int compare_by_name(const void* a, const void* b) {
    Student* studentA = (Student*) a;
    Student* studentB = (Student*) b;
    return strcmp(studentA->name, studentB->name);
}

int compare_by_reg_num(const void* a, const void* b) {
    Student* studentA = (Student*) a;
    Student* studentB = (Student*) b;
    return strcmp(studentA->reg_num, studentB->reg_num);
}

void sort_students(Student* students, int num_students) {
    if(num_students == 0) {
        printf("No students to sort.\n\n");
        return;
    }
    int choice;
    printf("1. Sort by name\n");
    printf("2. Sort by registration number\n");
    printf("Enter your choice: ");
    scanf("%d", &choice);

    switch(choice) {
        case 1:
            qsort(students, num_students, sizeof(Student), compare_by_name);
            printf("Students sorted by name:\n");
            for(int i = 0; i < num_students; i++) {
                printf("%s\n", students[i].name);
            }
            break;
        case 2:
            qsort(students, num_students, sizeof(Student), compare_by_reg_num);
            printf("Students sorted by registration number:\n");
            for(int i = 0; i < num_students; i++) {
                printf("%s\n", students[i].reg_num);
            }
            break;
        default:
            printf("Invalid choice. Please enter a number between 1 and 2.\n\n  ");
    }
}

void export_students(Student* students, int num_students) {
    if(num_students == 0) {
        printf("No students to export.\n");
        return;
    }
    FILE* file = fopen("students.csv", "a");
    if(file == NULL) {
        printf("Error opening file.\n");
        return;
    }
    for(int i = 0; i < num_students; i++) {
        fprintf(file, "%s,%s,%s,%s,%.2lf\n", students[i].name, students[i].dob, students[i].reg_num, students[i].prog_code, students[i].annual_tuition);
    }
    fclose(file);
    printf("Students exported to students.csv.\n");
}

int main() {
    // Create an instance of Student
    // Student s1;

    // Assign values to the fields
    // strcpy(s1.name, "Firidaus Kagolo");
    // strcpy(s1.dob, "2002-04-10");
    // strcpy(s1.reg_num, "247");
    // strcpy(s1.prog_code, "CSIT");
    // s1.annual_tuition = 2000000.0;

    // Print the values
    // printf("Name: %s\n", s1.name);
    // printf("DOB: %s\n", s1.dob);
    // printf("Registration Number: %06s\n", s1.reg_num);
    // printf("Program Code: %s\n", s1.prog_code);
    // printf("Annual Tuition: %.2f\n", s1.annual_tuition);


    Student students[100]; // Array to hold up to 100 students
    int num_students = 0; // Number of students currently in the array

    int choice;
    do {
        printf("Student Data App\n\n");
        printf("1. Create student\n");
        printf("2. Read student data\n");
        printf("3. Update student data\n");
        printf("4. Delete student\n");
        printf("5. Search student by registration number\n");
        printf("6. Sort students\n");
        printf("7. Export students to CSV\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch(choice) {
            case 1:
                create_student(students, &num_students);
                break;
            case 2:
                read_student(students, num_students);
                break;
            case 3:
                update_student(students, num_students);
                break;
            case 4:
                delete_student(students, &num_students);
                break;
            case 5:
                search_student(students, num_students);
                break;
            case 6:
                sort_students(students, num_students);
                break;
            case 7:
                export_students(students, num_students);
                break;
            case 8:
                printf("Exiting...\n");
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 8.\n");
        }
    } while(choice != 8);

    return 0;
}


